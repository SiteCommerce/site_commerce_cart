<?php

/**
 * @file
 * Contains site_commerce_cart.page.inc.
 *
 * Page callback for Cart entities.
 */

/**
 * Implements template_preprocess_site_commerce_cart_page().
 */
function template_preprocess_site_commerce_cart_page(&$variables) {
    // Шаблон блока с товарами в корзине.
    $elements = [
      '#theme' => 'site_commerce_cart_details',
    ];
    $variables['cart_details'] = $elements;
}

/**
 * Implements template_preprocess_site_commerce_cart_details().
 */
function template_preprocess_site_commerce_cart_details(&$variables) {
  $database = \Drupal::service('site_commerce_cart.database');

  $variables['positions_in_cart'] = $database->hasPositionsInCart();
  if ($variables['positions_in_cart']) {
    // Загружает массив с данными корзины.
    $variables['products'] = $database->loadCart();

    // Шаблон блока с информацией о стоимости заказа в корзине.
    $elements = [
      '#theme' => 'site_commerce_cart_order_information',
    ];
    $variables['order_information'] = $elements;
  } else {
    $elements = [
      '#theme' => 'site_commerce_cart_empty',
    ];
    $variables['cart_empty'] = $elements;
  }
}

/**
 * Implements template_preprocess_site_commerce_cart_order_information().
 */
function template_preprocess_site_commerce_cart_order_information(&$variables) {
  $database = \Drupal::service('site_commerce_cart.database');

  // Загружаем конфигурацию.
  $config = \Drupal::config('site_commerce_order.settings');

  // Определяем стоимость и валюту.
  $variables['order_cost'] = $database->getOrderCost(0, FALSE);
  $variables['default_currency_code'] = $config->get('default_currency_code') ? $config->get('default_currency_code') : "RUB";

  // Определяем символ валюты.
  $manager = \Drupal::entityTypeManager()
  ->getStorage('site_commerce_currency')
  ->loadByProperties(['letter_code' => $variables['default_currency_code']]);
  $currency = reset($manager);
  $variables['symbol'] = $currency->getSymbol();
}
