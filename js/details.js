/**
 * @file
 * Misc JQuery scripts in this file
 */
(function($, Drupal, drupalSettings) {
	'use strict';

	// // Добавляет параметра позиции в корзину.
	// $('.add-cart-item').on('click', function() {
	// 	var type = $(this).attr('parametr-type');
	// 	var id = parseInt($(this).attr('parametr-id'));
	// 	var delta = parseInt($(this).attr('parametr-delta'));

	// 	var input_quantity = $('#product-parametrs__quantity-' + delta);
	// 	var min = parseInt(input_quantity.attr('min'));
	// 	var quantity = parseInt(input_quantity.val().trim());
	// 	if (isNaN(quantity) || quantity < min) {
	// 		quantity = min;
	// 		input_quantity.val(quantity);
	// 	}

	// 	if (quantity) {
	// 		// Выполняет запрос ajax.
	// 		var ajaxObject = Drupal.ajax({
	// 			url: '/add-cart-item/' + type + '/' + id + '/' + delta + '/' + quantity + '/nojs',
	// 			base: false,
	// 			element: false,
	// 			progress: false,
	// 			cache: false
	// 		});
	// 		ajaxObject.execute();
	// 	}
	// });

	// Регистрирует изменение кол-ва товаров в корзине.
	$('.cart__item-quantity-value').on('change', function() {
		var cid = parseInt($(this).attr('cid'));
		var min = parseInt($(this).attr('min'));
		var quantity = parseInt($(this).val().trim());
		if (isNaN(quantity) || quantity < min) {
			quantity = min;
			$(this).val(quantity);
		}

		if (cid && quantity) {
			updateQuantity(cid, quantity);
		}
	});

	// Регистрирует изменение кол-ва товаров в корзине при нажатии на кнопку изменения количества.
	$('.cart__item-quantity-actions-button').on('click', function() {
		var cid = parseInt($(this).attr('cid'));
		var min = parseInt($('#cart__item-quantity-value-' + cid).attr('min'));
		var quantity = parseInt($('#cart__item-quantity-value-' + cid).val().trim());
		if (isNaN(quantity) || quantity < min) {
			quantity = min;
			$('#cart__item-quantity-value-' + cid).val(quantity);
		}

		if (cid && quantity) {
			updateQuantity(cid, quantity);
		}
	});

	function updateQuantity(cid, quantity) {
		// Устанавливаем прелоадер.
		if (typeof drupalSettings.ajaxLoader != 'undefined') {
			let element = '<div class="site-orders-cart-ajax-preloader">' + drupalSettings.ajaxLoader.markup + '</div>';
			$('.order-information__preloader').html(element);
		}

		// Выполняет запрос ajax.
		var ajaxObject = Drupal.ajax({
			url: '/cart-item-update-quantity/' + cid + '/' + quantity + '/nojs',
			base: false,
			element: false,
			progress: false
		});
		ajaxObject.execute();
	}

	// Удаляет позицию из корзины.
	$('.cart__item-delete').on('click', function() {
		var cid = parseInt($(this).attr('cid'));
		if (cid) {
			// Устанавливаем прелоадер.
			if (typeof drupalSettings.ajaxLoader != 'undefined') {
			  let element = '<div class="site-orders-cart-ajax-preloader">' + drupalSettings.ajaxLoader.markup + '</div>';
			  $('#cart__item-actions-' + cid).html(element);
			}

			// Выполняет запрос ajax.
			var ajaxObject = Drupal.ajax({
				url: '/cart-item-delete/' + cid + '/nojs',
				base: false,
				element: false,
				progress: false
			});
			ajaxObject.execute();
		}
	});
})(jQuery, Drupal, drupalSettings);
