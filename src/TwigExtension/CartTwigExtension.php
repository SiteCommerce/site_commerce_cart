<?php

namespace Drupal\site_commerce_cart\TwigExtension;

use Drupal\site_commerce_cart\Form\CartProductAddForm;
use Drupal\site_commerce_product\Entity\ProductInterface;

/**
 * Twig extension that adds a custom function and a custom filter.
 */
class CartTwigExtension extends \Twig_Extension {

  /**
   * In this function we can declare the extension function
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('cart_product_add_form', [$this, 'cartProductAddForm']),
      new \Twig_SimpleFunction('goto_order_checkout_form', [$this, 'gotoOrderCheckoutForm']),
    ];
  }

  /**
   * Gets a unique identifier for this Twig extension.
   *
   * @return string
   *   A unique identifier for this Twig extension.
   */
  public function getName() {
    return 'site_commerce_cart.twig_extension';
  }

  /**
   * Формирует форму добавить в корзину.
   */
  public static function cartProductAddForm(ProductInterface $entity, bool $catalog = FALSE) {
    $form = new CartProductAddForm($entity, $catalog);
    $form = \Drupal::formBuilder()->getForm($form);
    return $form;
  }

  /**
   * Форма перехода к странице оформления заказа.
   */
  public static function gotoOrderCheckoutForm() {
    $form = \Drupal::formBuilder()->getForm('Drupal\site_commerce_cart\Form\CartGotoOrderCheckoutForm');
    return $form;
  }
}
