<?php

namespace Drupal\site_commerce_cart\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Cart entity.
 *
 * @ingroup site_commerce_cart
 *
 * @ContentEntityType(
 *   id = "site_commerce_cart",
 *   label = @Translation("Cart item"),
 *   label_collection = @Translation("Cart items"),
 *   label_singular = @Translation("cart item"),
 *   label_plural = @Translation("cart items"),
 *   label_count = @PluralTranslation(
 *     singular = "@count cart item",
 *     plural = "@count cart items",
 *   ),
 *   bundle_label = @Translation("Cart item type"),
 *   handlers = {
 *     "list_builder" = "Drupal\site_commerce_cart\CartListBuilder",
 *     "views_data" = "Drupal\site_commerce_cart\Entity\CartViewsData",
 *     "access" = "Drupal\site_commerce_cart\CartAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\site_commerce_cart\Form\CartForm",
 *       "edit" = "Drupal\site_commerce_cart\Form\CartForm",
 *       "delete" = "Drupal\site_commerce_cart\Form\CartDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\site_commerce_cart\CartHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "site_commerce_cart",
 *   admin_permission = "administer site_commerce_cart",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "cart_id",
 *     "bundle" = "type",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "label" = "title",
 *   },
 *   links = {
 *     "edit-form" = "/admin/site-commerce/shopping-list/{site_commerce_cart}/edit",
 *     "delete-form" = "/admin/site-commerce/shopping-list/{site_commerce_cart}/delete",
 *     "collection" = "/admin/site-commerce/shopping-list",
 *   },
 *   bundle_entity_type = "site_commerce_cart_type",
 *   field_ui_base_route = "entity.site_commerce_cart_type.edit_form"
 * )
 */
class Cart extends ContentEntityBase implements CartInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCartItemEntityTypeId() {
    return $this->get('entity_type_id')->getString();
  }

  /**
   * {@inheritdoc}
   */
  public function getCartItemEntityId() {
    return (int) $this->get('entity_id')->getString();
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuantity() {
    return $this->get('quantity')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setQuantity(int $value) {
    $this->set('quantity', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Customer'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 0,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['qrsales_uuid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('ID of the unauthorized сustomer'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['order_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Order'))
      ->setSetting('target_type', 'site_commerce_order')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 0,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['entity_type_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Type of product'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSetting('max_length', 255);

    $fields['entity_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Product'))
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Product name'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['quantity'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Quantity'))
      ->setSetting('unsigned', TRUE)
      ->setDefaultValue(1)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number_integer',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['price'] = BaseFieldDefinition::create('site_commerce_price')
      ->setLabel(t('Price'))
      ->setDescription(t('Cost per unit.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Show in the cart'))
      ->setDescription(t('Status that allows displaying the item in the cart. When deleting a position, the status is set to 0 and is not taken into account in the cost calculations and the order.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 0,
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('Date the item was created in the cart.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('Date the item was edited in the cart.'));

    return $fields;
  }
}
