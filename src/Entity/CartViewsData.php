<?php

namespace Drupal\site_commerce_cart\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Cart entities.
 */
class CartViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();
    return $data;
  }
}
