<?php

namespace Drupal\site_commerce_cart\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Cart entities.
 *
 * @ingroup site_commerce_cart
 */
interface CartInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the cart item entity_type_id.
   *
   * @return string
   *   Name of the entity type.
   */
  public function getCartItemEntityTypeId();

  /**
   * Gets the cart item entity_id.
   *
   * @return int
   *   ID of the entity cart item.
   */
  public function getCartItemEntityId();

  /**
   * Gets the cart item name.
   *
   * @return string
   *   Name of the cart item.
   */
  public function getTitle();

  /**
   * Sets the cart item name.
   *
   * @param string $title
   *   The cart item.
   *
   * @return \Drupal\site_commerce_cart\Entity\CartInterface
   *   The called Cart entity.
   */
  public function setTitle($title);

  /**
   * Gets the quantity in cart.
   *
   * @return int
   *   Quantity in cart.
   */
  public function getQuantity();

  /**
   * Sets the quantity in cart.
   *
   * @param int $value
   *   Quantity in cart.
   *
   * @return \Drupal\site_commerce_cart\Entity\CartInterface
   *   The called Cart entity.   *
   */
  public function setQuantity(int $value);

  /**
   * Gets the Cart creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Cart.
   */
  public function getCreatedTime();

  /**
   * Sets the Cart creation timestamp.
   *
   * @param int $timestamp
   *   The Cart creation timestamp.
   *
   * @return \Drupal\site_commerce_cart\Entity\CartInterface
   *   The called Cart entity.
   */
  public function setCreatedTime($timestamp);

}
