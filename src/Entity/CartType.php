<?php
namespace Drupal\site_commerce_cart\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Cart configuration entity.
 *
 * @ConfigEntityType(
 *   id = "site_commerce_cart_type",
 *   label = @Translation("Cart item type"),
 *   label_collection = @Translation("Cart item types"),
 *   label_singular = @Translation("cart item type"),
 *   label_plural = @Translation("cart item types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count cart item type",
 *     plural = "@count cart item types",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\site_commerce_cart\CartTypeListBuilder",
 *     "form" = {
 *       "default" = "Drupal\site_commerce_cart\Form\CartTypeForm",
 *       "add" = "Drupal\site_commerce_cart\Form\CartTypeForm",
 *       "edit" = "Drupal\site_commerce_cart\Form\CartTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\site_commerce_cart\CartTypeHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer site commerce",
 *   config_prefix = "site_commerce_cart_type",
 *   bundle_of = "site_commerce_cart",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label"
 *   },
 *   links = {
 *     "canonical" = "/admin/site-commerce/config/cart/cart-types/{site_commerce_cart_type}",
 *     "add-form" = "/admin/site-commerce/config/cart/cart-types/add",
 *     "edit-form" = "/admin/site-commerce/config/cart/cart-types/{site_commerce_cart_type}/edit",
 *     "delete-form" = "/admin/site-commerce/config/cart/cart-types/{site_commerce_cart_type}/delete",
 *     "collection" = "/admin/site-commerce/config/cart/cart-types",
 *   }
 * )
 */
class CartType extends ConfigEntityBundleBase {
  /**
   * The entity ID.
   *
   * @var string
   */
  public $id;

  /**
   * The entity label.
   *
   * @var string
   */
  public $label;
}
