<?php

namespace Drupal\site_commerce_cart\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CartGotoOrderCheckoutForm.
 */
class CartGotoOrderCheckoutForm extends FormBase {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'site_commerce_cart_goto_order_checkout_form';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Кнопка отправки формы.
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create order'),
      '#button_type' => 'primary',
      '#attributes' => ['class' => ['site-orders-goto-order-checkout-form__btn', 'site-orders-goto-order-checkout-form__btn-submit', 'site-orders-goto-order-checkout-form__btn-no-background']],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('site_commerce_order.checkout');
  }
}
