<?php

namespace Drupal\site_commerce_cart\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\site_commerce_product\Entity\ProductInterface;

/**
 * Class add product to cart.
 */
class CartProductAddForm extends FormBase {

  /**
   * Сущность позиции для добавления в корзину.
   */
  protected $entity;

  /**
   * Признак наличия товара в корзине.
   * @var bool
   */
  protected $position_in_cart;

  /**
   * Признак запрета добавления товара в корзину.
   * @var bool
   */
  protected $allow_add_to_cart = TRUE;

  /**
   * Признак отображения формы в каталоге товаров.
   * @var bool
   */
  protected $catalog;

  /**
   * Текущее количество на складе.
   * @var int
   */
  protected $stock_value;

  /**
   * Параметр определяет, что текущая цена товара больше нуля если TRUE.
   * По-умолчанию считается, что цена товара равна нулю.
   * @var bool
   */
  protected $price_exist = FALSE;

  /**
   * Конструктор класса.
   * @param ProductInterface $entity
   * @return void
   */
  public function __construct(ProductInterface $entity, bool $catalog = FALSE) {
    $databaseCart = \Drupal::service('site_commerce_cart.database');

    // Сущность товара.
    $this->entity = $entity;

    // Определяем существует ли цена у товара.
    $price = $entity->getPrice();
    $price_number = bccomp($price['number_raw'], 0, 10);
    $price_number_from = bccomp($price['number_from_raw'], 0, 10);
    if ($price_number && !$price_number_from) {
      $this->price_exist = TRUE;
    }

    // Если разрешено учитывать количество на складе.
    if ($entity->getStockAllow()) {
      $this->stock_value = (int) $entity->getStockValue();
    } else {
      $this->stock_value = 1;
    }

    $this->catalog = $catalog;

    // Проверяем добавлен ли товар в корзину.
    $this->position_in_cart = FALSE;
    if ($databaseCart->hasPositionInCart($entity->getEntityTypeId(), $entity->id())) {
      $this->position_in_cart = TRUE;
    }
  }

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'site_commerce_cart_product_add_form_' . $this->entity->id();
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Сущность товара.
    $entity = $this->entity;

    // Проверяем добавлен ли товар в корзину.
    $position_in_cart = $this->position_in_cart;

    $form['#id'] = Html::getId($this->getFormId());

    $form_class = Html::getClass('site_commerce_cart_product_add_form');

    // Обертка элементов формы.
    $form['wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => $form_class . '__wrapper-' . $entity->id(),
      ],
    ];

    // Добавляем нашу кнопку для отправки.
    $form['wrapper']['actions']['#type'] = 'actions';
    $form['wrapper']['actions']['#attributes'] = ['class' => [$form_class . '__actions']];
    $form['wrapper']['actions']['#weight'] = 5;

    // Кнопка быстрого оофрмления заказа.
    $cart_label_button_click = trim($entity->get('field_settings')->cart_label_button_click);
    $form['wrapper']['actions']['buy_one_click'] = [
      '#type' => 'submit',
      '#value' => $this->t($cart_label_button_click),
      '#button_type' => 'primary',
      '#ajax' => [
        'callback' => '::ajaxBuyOneClickCallback',
      ],
      '#attributes' => [
        'class' => [$form_class . '__btn', $form_class . '__btn-buy-one-click', $form_class . '__btn-no-background'],
      ],
      '#access' => $this->price_exist && !$this->catalog && $cart_label_button_click && $this->stock_value ? TRUE : FALSE,
    ];

    // Название кнопки добавления в корзину.
    // Если отображается форма в каталоге.
    if ($this->catalog) {
      if ($position_in_cart) {
        $value_add_to_cart = $this->t('In the cart');
      } else {
        $value_add_to_cart = $this->t('To the cart');
      }
    } else {
      $cart_label_button_add = trim($entity->get('field_settings')->cart_label_button_add);
      if (empty($cart_label_button_add)) {
        if ($position_in_cart) {
          $value_add_to_cart = $this->t('Go to cart');
        } else {
          $value_add_to_cart = $this->t('Add to cart');
        }
      } else {
        $value_add_to_cart = $this->t($cart_label_button_add);
      }
    }

    // Кнопка добавления в корзину.
    $form['wrapper']['actions']['add_to_cart'] = [
      '#type' => 'submit',
      '#id' => $form_class . '__btn-add-to-cart-' . $entity->id(),
      '#value' => $value_add_to_cart,
      '#button_type' => 'primary',
      '#ajax' => [
        'callback' => $position_in_cart ? '::ajaxGotoCartCallback' : '::ajaxAddToCartCallback',
      ],
      '#attributes' => [
        'class' => [$form_class . '__btn', $form_class . '__btn-add-to-cart'],
      ],
      '#access' => $this->price_exist && $this->stock_value ? TRUE : FALSE,
    ];

    if ($position_in_cart) {
      $form['wrapper']['actions']['add_to_cart']['#attributes']['class'][] = $form_class . '__btn-no-background';
    }

    if ($this->catalog) {
      $form['wrapper']['actions']['add_to_cart']['#attributes']['class'][] = $form_class . '__btn-catalog';
    }

    // Обертка элементов формы.
    $form['wrapper']['actions']['not_in_stock'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => $form_class . '__not-in-stock',
      ],
      '#markup' => $this->t('The product is not in stock'),
      '#access' => $this->stock_value ? FALSE : TRUE,
    ];

    // Элемент для вывода сообщений об ошибках и уведомлений.
    $form['wrapper']['message'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => $form_class . '__add-to-cart-validation-message-' . $entity->id(),
      ],
      '#weight' => 10,
    ];

    $form['#attached']['library'][] = 'site_commerce_cart/product_add_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Позиция товара.
    $entity = $this->entity;
    if (!$this->position_in_cart && $entity->getStockAllow() && !$entity->getStockValue()) {
      $form_state->setErrorByName('add_to_cart', $this->t('The product is not in stock.'));
      $this->allow_add_to_cart = FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Если товар отсутствует в корзине.
    if (!$this->position_in_cart) {
      // Позиция товара.
      $entity = $this->entity;

      // Количество товара в заказе.
      $quantity = 1;
      if ($entity->get('field_settings')->quantity_order_min > 1) {
        $quantity = (int) $entity->get('field_settings')->quantity_order_min;
      }

      // Изменяем кол-во остатка на складе
      // если разрешено учитывать количество для товара.
      if ($entity->getStockAllow()) {
        // Текущее количество на складе.
        if (!$this->stock_value) {
          $this->allow_add_to_cart = FALSE;
          return FALSE;
        }

        // Если количество для заказа больше остатков на складе.
        if ($quantity > $this->stock_value) {
          $quantity = $this->stock_value;
        }

        // Изменяем количество товара на складе.
        $value = $this->stock_value - $quantity;
        $entity->setStockValue($value);
        $entity->save();
      }

      // Стоимость позиции для текущего пользователя.
      $cost = $entity->getPrice();

      // Регистрирует позицию в корзине.
      $databaseCart = \Drupal::service('site_commerce_cart.database');
      $databaseCart->addToCart(
        $entity->getEntityTypeId(),
        $entity->id(),
        $entity->label(),
        $quantity,
        $cost['number_raw'],
        $cost['currency_code']
      );

      $this->position_in_cart = TRUE;
    }

    $form_state->setRebuild(TRUE);
  }

  /**
   * Ajax callback for buy one click button.
   */
  public function ajaxBuyOneClickCallback(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    if (!$form_state->hasAnyErrors()) {
      // Устанавливаем редирект на страницу оформления заказа.
      $url = Url::fromRoute('site_commerce_order.checkout');
      $command = new RedirectCommand($url->toString());
      $response->addCommand($command);
    }

    return $response;
  }

  /**
   * Ajax callback for product in the cart click button.
   */
  public function ajaxGotoCartCallback(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    if (!$form_state->hasAnyErrors()) {
      // Устанавливаем редирект на страницу корзины.
      $url = Url::fromRoute('site_commerce_cart.cart_page');
      $command = new RedirectCommand($url->toString());
      $response->addCommand($command);
    }

    return $response;
  }

  /**
   * Ajax add to cart product callback.
   */
  public function ajaxAddToCartCallback(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    // Позиция товара.
    $entity = $this->entity;

    // Удаляем ошибки валидации формы.
    $this->messenger()->deleteAll();

    // Обновление формы.
    $response->addCommand(new ReplaceCommand('#site-commerce-cart-product-add-form__wrapper-' . $entity->id(), $form['wrapper']));

    return $response;
  }
}
