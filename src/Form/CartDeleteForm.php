<?php

namespace Drupal\site_commerce_cart\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Cart entities.
 *
 * @ingroup site_commerce_cart
 */
class CartDeleteForm extends ContentEntityDeleteForm {

}
