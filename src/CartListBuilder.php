<?php

namespace Drupal\site_commerce_cart;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Cart entities.
 *
 * @ingroup site_commerce_cart
 */
class CartListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Cart ID');
    $header['name'] = $this->t('Title');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\site_commerce_cart\Entity\Cart $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.site_commerce_cart.edit_form',
      ['site_commerce_cart' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
