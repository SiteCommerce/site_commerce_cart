<?php

namespace Drupal\site_commerce_cart\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\site_commerce_cart\Entity\CartInterface;
use Drupal\site_commerce_product\Entity\ProductInterface;

/**
 * Возврат на склад неоформленных в заказ товаров.
 *
 * @QueueWorker(
 *   id = "site_commerce_cart_products_return_to_storage",
 *   title = @Translation("Return to the storage of unformed products in the order"),
 *   cron = {"time" = 60}
 * )
 */
class ProductsReturnToStorage extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $cart_id = (int) $data['cart_id'];

    // Загружаем позицию товара.
    $cart = \Drupal::entityTypeManager()->getStorage('site_commerce_cart')->load($cart_id);
    if ($cart instanceof CartInterface) {
      // Текущее количество в корзине.
      $cart_quantity = (int) $cart->getQuantity();

      // Параметры товара.
      $entity_type_id = $cart->getCartItemEntityTypeId();
      $entity_id = $cart->getCartItemEntityId();

      // Обновляем остатки на складе.
      $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($entity_id);
      if ($entity instanceof ProductInterface && $entity->getStockAllow()) {
        // Текущее количество на складе.
        $stock_value = (int) $entity->getStockValue();

        // Изменяем количество товара на складе.
        $value = $stock_value + $cart_quantity;

        // Сохраняем количество товара на складе.
        $entity->setStockValue($value);
        $entity->save();
      }

      // Удаляем позицию в корзине.
      $cart->delete();
    }
  }
}
