<?php

namespace Drupal\site_commerce_cart\Controller;

use Drupal\Core\Controller\ControllerBase;

class CartController extends ControllerBase {

  /**
   * Страница c корзиной.
   */
  public function cartPage() {
    $noindex_meta_tag = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'robots',
        'content' => 'noindex, nofollow',
      ],
    ];

    return [
      '#theme' => 'site_commerce_cart_page',
      '#attached' => [
        'library' => [],
        'html_head' => [[$noindex_meta_tag, 'noindex']],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }
}
