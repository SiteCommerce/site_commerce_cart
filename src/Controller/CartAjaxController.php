<?php

namespace Drupal\site_commerce_cart\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\site_commerce_cart\Controller\CartDatabaseController;
use Drupal\site_commerce_cart\Entity\CartInterface;
use Drupal\site_commerce_product\Entity\ProductInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CartAjaxController extends ControllerBase {

  /**
   * The servises classes.
   *
   * @var \Drupal\site_commerce_cart\Controller\CartDatabaseController
   */
  protected $databaseCart;

  /**
   * Constructs.
   *
   * @param \Drupal\site_commerce_cart\Controller\CartDatabaseController $connection
   *   The database connection.
   */
  public function __construct(CartDatabaseController $databaseCart) {
    $this->databaseCart = $databaseCart;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('site_commerce_cart.database')
    );
  }

  /**
   * Обновляет значение количества в корзине.
   *
   * @param [type] $method
   * @param [type] $cart_id
   * @param [type] $quantity
   * @return void
   */
  public function cartItemUpdateQuantity(int $cart_id, int $quantity, string $method) {
    if ($method == 'ajax') {
      // Create AJAX Response object.
      $response = new AjaxResponse();

      $cart = \Drupal::entityTypeManager()->getStorage('site_commerce_cart')->load($cart_id);
      if ($cart instanceof CartInterface) {
        // Текущее количество в корзине.
        $cart_quantity = (int) $cart->getQuantity();

        // Параметры товара.
        $entity_type_id = $cart->getCartItemEntityTypeId();
        $entity_id = $cart->getCartItemEntityId();

        // Обновляем остатки на складе.
        $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($entity_id);
        if ($entity instanceof ProductInterface && $entity->getStockAllow()) {
          // Текущее количество на складе.
          $stock_value = (int) $entity->getStockValue();

          // Если количество увеличивается.
          if ($quantity > $cart_quantity) {
            // Количество товара, хотим взять со склада.
            $delta = $quantity - $cart_quantity;
            if ($delta > $stock_value) {
              $delta = $stock_value;
            }

            // Не можем увеличить больше чем доступно на складе.
            $quantity = $delta + $cart_quantity;

            // Изменяем количество товара на складе.
            $value = $stock_value - $delta;
          }

          // Если количество уменьшается.
          if ($quantity < $cart_quantity) {
            // Количество товара, хотим вернуть на склад.
            $delta = $cart_quantity - $quantity;

            // Изменяем количество товара на складе.
            $value = $stock_value + $delta;
          }

          // Сохраняем количество товара на складе.
          $entity->setStockValue($value);
          $entity->save();
        }

        // Обновляем количество в корзине.
        $cart->setQuantity($quantity);
        $cart->save();
      }

      // Пересчитываем информацию о заказе.
      $elements = [
        '#theme' => 'site_commerce_cart_order_information',
      ];
      $response->addCommand(new HtmlCommand('.cart__order-information', $elements));

      // Return ajax response.
      return $response;
    }
  }

  /**
   * Удаляет позицию в корзине.
   *
   * @param [type] $method
   * @param [type] $cid
   * @param [type] $quantity
   * @return void
   */
  public function cartItemDelete(int $cart_id, string $method) {
    if ($method == 'ajax') {
      // Create AJAX Response object.
      $response = new AjaxResponse();

      $cart = \Drupal::entityTypeManager()->getStorage('site_commerce_cart')->load($cart_id);
      $value = 0;
      $stock_value = 0;
      $entity = NULL;
      if ($cart instanceof CartInterface) {
        // Текущее количество в корзине.
        $cart_quantity = (int) $cart->getQuantity();
        $entity_type_id = $cart->getCartItemEntityTypeId();
        $entity_id = $cart->getCartItemEntityId();

        // Обновляем остатки на складе.
        $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($entity_id);
        if ($entity instanceof ProductInterface && $entity->getStockAllow()) {
          // Текущее и новое количество на складе.
          $stock_value = (int) $entity->getStockValue();
          $value = $stock_value + $cart_quantity;
        }

        // Удаляем позицию из корзины.
        $cart->delete();

        // Обновляем количество товара на складе.
        if ($entity instanceof ProductInterface && $value > $stock_value) {
          $entity->setStockValue($value);
          $entity->save();
        }

        $response->addCommand(new RemoveCommand('#cart__item-' . $cart_id));
      }

      if ($this->databaseCart->hasPositionsInCart()) {
        // Пересчитываем информацию о заказе.
        $elements = [
          '#theme' => 'site_commerce_cart_order_information',
        ];
        $response->addCommand(new HtmlCommand('.cart__order-information', $elements));
      } else {
        $elements = [
          '#theme' => 'site_commerce_cart_empty',
        ];
        $response->addCommand(new HtmlCommand('#cart-wrapper', $elements));
      }

      // Return ajax response.
      return $response;
    }
  }
}
