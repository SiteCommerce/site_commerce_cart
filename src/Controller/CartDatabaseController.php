<?php

namespace Drupal\site_commerce_cart\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\site_commerce_product\Entity\ProductInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Database class.
 */
class CartDatabaseController extends ControllerBase {
  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new SiteOrdersDatabaseController.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection, EntityTypeManagerInterface $entity_type_manager) {
    $this->connection = $connection;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Добавляет позицию в корзину.
   */
  public function addToCart(string $entity_type_id, int $entity_id, string $label, int $quantity, float $number, string $currency_code) {
    if (!$this->hasPositionInCart($entity_type_id, $entity_id)) {
      $account = \Drupal::currentUser();

      $entity = \Drupal::entityTypeManager()->getStorage('site_commerce_cart')->create([
        'type' => 'default',
        'status' => 1,
        'entity_type_id' => $entity_type_id,
        'entity_id' => $entity_id,
        'order_id' => 0,
        'uid' => $account->id(),
        'qrsales_uuid' => kvantstudio_user_hash(),
        'title' => $label,
        'quantity' => $quantity,
        'price' => [
          'number' => $number,
          'currency_code' => $currency_code,
        ],
        'created' => \Drupal::time()->getRequestTime(),
        'changed' => 0,
      ]);
      $entity->enforceIsNew(TRUE);
      $entity->save();

      return $entity;
    } else {
      return FALSE;
    }
  }

  /**
   * Проверяет наличие позиции в корзине, когда она еще не оформлена в заказ.
   */
  public function hasPositionInCart(string $entity_type_id, int $entity_id) {
    $account = \Drupal::currentUser();

    $query = $this->connection->select('site_commerce_cart', 'n');
    $query->addExpression('COUNT(n.cart_id)');
    $query->condition('n.order_id', 0);
    $query->condition('n.entity_type_id', $entity_type_id);
    $query->condition('n.entity_id', $entity_id);

    if ($account->id()) {
      $query->condition('n.uid', $account->id());
    } else {
      $query->condition('n.qrsales_uuid', kvantstudio_user_hash());
    }

    return (bool) $query->execute()->fetchField();
  }

  /**
   * Проверяет наличие всех позициий в корзине, когда она еще не оформлена в заказ.
   */
  public function hasPositionsInCart() {
    $account = \Drupal::currentUser();

    $query = $this->connection->select('site_commerce_cart', 'n');
    $query->addExpression('COUNT(n.cart_id)');
    $query->condition('n.order_id', 0);

    if ($account->id()) {
      $query->condition('n.uid', $account->id());
    } else {
      $query->condition('n.qrsales_uuid', kvantstudio_user_hash());
    }

    return (bool) $query->execute()->fetchField();
  }

  /**
   * Загружает массив с данными корзины. По умолчанию загружаются данные не оформленные в заказ.
   */
  public function loadCart(int $order_id = 0) {
    $account = \Drupal::currentUser();
    $query = $this->connection->select('site_commerce_cart', 'n');
    $query->fields('n');
    $query->condition('n.order_id', $order_id);

    // Определяем позиции добавленные текущим пользователем.
    if (!$order_id) {
      if ($account->id()) {
        $query->condition('n.uid', $account->id());
      } else {
        $query->condition('n.qrsales_uuid', kvantstudio_user_hash());
      }
    }

    $result = $query->execute();

    $data = [];
    $style = \Drupal::entityTypeManager()->getStorage('image_style')->load('product_card_image');
    foreach ($result as $row) {
      // Если тип позиции карточка товара.
      if ($row->entity_type_id === 'site_commerce_product') {
        $site_commerce = $this->entityTypeManager->getStorage('site_commerce_product')->load($row->entity_id);
        if ($site_commerce instanceof ProductInterface) {

          // Если существует изображение товара.
          // TODO: Добавить поддержку изображений по умолчанию при его отсутствии.
          $style_url = '';
          if (!$site_commerce->get('field_media_image')->isEmpty()) {
            $field_media_image = $site_commerce->get('field_media_image')->first()->getValue();
            if (isset($field_media_image['target_id'])) {
              $media = Media::load($field_media_image['target_id']);
              $media_field = $media->get('field_media_image')->first()->getValue();
              $file = File::load($media_field['target_id']);
              if ($file) {
                $uri = $file->getFileUri();
                $style_url = $style->buildUrl($uri);
              }
            }
          }

          // Определяем символ валюты.
          $manager = \Drupal::entityTypeManager()
            ->getStorage('site_commerce_currency')
            ->loadByProperties(['letter_code' => $row->price__currency_code]);
          $currency = reset($manager);

          $data[$row->cart_id] = [
            'image_url' => $style_url,
            'entity_id' => $row->entity_id,
            'url' => $site_commerce->toUrl()->toString(),
            'title' => $site_commerce->getTitle(),
            'quantity_visible' => $site_commerce->getStockAllow(),
            'quantity_cart' => $row->quantity,
            'quantity_min' => $site_commerce->getQuantityOrderMin(),
            'quantity_unit' => $site_commerce->getQuantityUnit(),
            'price' => \Drupal::service('kvantstudio.formatter')->price($row->price__number),
            'currency_code' => \Drupal::service('kvantstudio.formatter')->price($row->price__currency_code),
            'symbol' => $currency->getSymbol(),
          ];
        } else {
          // Объект позиции не может быть загружен.
          // Удаление его из корзины.
          $cart = \Drupal::entityTypeManager()->getStorage('site_commerce_cart')->load($row->cart_id);
          $cart->delete();
        }
      }
    }

    return $data;
  }

  /**
   * Загружает стоимость заказа по стоимости позиций в корзине.
   */
  public function getOrderCost(int $order_id = 0, bool $trailing_zeros = TRUE) {
    $account = \Drupal::currentUser();
    $query = $this->connection->select('site_commerce_cart', 'n');
    $query->fields('n', ['quantity', 'price__number']);
    $query->condition('n.order_id', $order_id);

    // Определяем позиции добавленные текущим пользователем.
    if (!$order_id) {
      if ($account->id()) {
        $query->condition('n.uid', $account->id());
      } else {
        $query->condition('n.qrsales_uuid', kvantstudio_user_hash());
      }
    }

    $result = $query->execute();

    $cost = 0;
    foreach ($result as $row) {
      $cost = $cost + ($row->quantity * $row->price__number);
    }

    if ($trailing_zeros) {
      return \Drupal::service('kvantstudio.formatter')->removeTrailingZeros($cost);
    }

    return \Drupal::service('kvantstudio.formatter')->price($cost);
  }

  /**
   * Перечень товаров не оформленных в заказ.
   */
  public function productsNotOrdered(int $period = 86400) {
    // Определяем максимальное время в течении которого товары могут находиться в корзине.
    // По умочанию 24 часа.
    $request_time = \Drupal::time()->getRequestTime();
    $timestamp = (int) ($request_time - $period);

    $query = $this->connection->select('site_commerce_cart', 'n');
    $query->fields('n', ['cart_id']);
    $query->condition('n.order_id', 0);
    $query->condition('n.created', $timestamp, '<');
    $query->condition('n.status', 1);
    return $query->execute()->fetchAll();
  }

}
