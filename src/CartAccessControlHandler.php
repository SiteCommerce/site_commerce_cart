<?php

namespace Drupal\site_commerce_cart;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Cart entity.
 *
 * @see \Drupal\site_commerce_cart\Entity\Cart.
 */
class CartAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\site_commerce_cart\Entity\CartInterface $entity */

    switch ($operation) {

      case 'edit':

        return AccessResult::allowedIfHasPermission($account, 'site commerce edit cart entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'site commerce delete cart entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'site commerce add cart entities');
  }
}
